FROM nginx

ENV PERL5LIB=/app/lib

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
        build-essential \
        cpanminus \
        libssl-dev \
        libxml2-dev \
        openssl \
        zlib1g-dev \
 && cpanm -q \
        Cpanel::JSON::XS \
        Data::Dumper::Concise \
        Function::Parameters \
        Gzip::Faster \
        HTTP::Tiny \
        Log::Any \
        Moo \
        IO::Socket::SSL \
        Path::Tiny \
        Ref::Util::XS \
        URL::Builder \
        XML::Writer::Simple \
 && rm -rf /root/.cpanm /var/lib/apt/lists

ARG GIT_SHA="Development version"
ENV GIT_SHA=$GIT_SHA

COPY image/ /
CMD /app/entrypoint.sh
