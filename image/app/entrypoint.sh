#!/bin/bash

HTMLDIR=/usr/share/nginx/html

echo Creating version files
echo "{\"version\":\"$GIT_SHA\"}" > $HTMLDIR/version.json
echo $GIT_SHA > $HTMLDIR/version

echo Fetching Museum Victoria feed...
/app/bin/convert-feed $HTMLDIR

echo Serving...
exec nginx -g 'daemon off;'
