package JsonToXml;

use 5.24.0;
use warnings;

use Ref::Util::XS           qw( is_ref is_arrayref is_hashref );
use XML::Writer::Simple     qw( );

use Function::Parameters    qw( :strict );

use parent qw( Exporter );

our @EXPORT_OK = qw( json_to_xml );

fun json_to_xml($json, $top_level_tag) {
    my $stripped = _strip_empty($json);

    my $xml;
    open(my $fh, '>', \$xml)
        or die "Opening string as filehandle: $!\n";

    my $writer = XML::Writer::Simple->new(fh => $fh);
    _convert_json({ $top_level_tag => $stripped }, $writer);
    close($fh);

    return $xml;
}

fun _convert_json($json, $writer) {
    for my $key (sort keys %$json) {
        my $value = $json->{$key};

        if (!is_ref($value)) {
            $writer->element($key, $value);
            next;
        }

        $writer->open($key);
        if (is_arrayref($value)) {
            for my $item (@$value) {
                _convert_json({ element => $item }, $writer);
            }
        }
        elsif (is_hashref($value)) {
            _convert_json($value, $writer);
        }
        else {
            die "Unexpected ", Dumper($value);
        }
        $writer->close;
    }
}

fun _strip_empty($in) {
    return $in unless defined $in;

    if (!is_ref($in)) {
        return $in =~ /\S/ ? $in : undef;
    }

    if (is_arrayref($in)) {
        my @out = map { _strip_empty($_) } @$in;
        return @out > 0 ? \@out : undef;
    }

    if (is_hashref($in)) {
        my %out;

        for my $key (keys %$in) {
            my $value = _strip_empty($in->{$key});
            if (defined $value) {
                $out{$key} = $value;
            }
        }

        return keys %out > 0 ? \%out : undef;
    }

    die "Unexpected ", Dumper($in);
}

1;
