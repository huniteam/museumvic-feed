package Museum::Feed;

use 5.24.0;
use warnings;

use Moo;

use Digest::MD5             qw( md5_hex );
use JsonToXml               qw( json_to_xml );
use Museum::API             qw( );
use Path::Tiny              qw( );
use XML::Writer::Simple     qw( );

use Function::Parameters    qw( :strict );

has api => (
    is => 'lazy',
    builder => sub { Museum::API->new },
);

has output_dir => (
    is => 'ro',
    required => 1,
    coerce => \&Path::Tiny::path,
);

has queries => (
    is => 'ro',
    default => sub { [ 'sunshine harvester' ] },
);

has record_types => (
    is => 'ro',
    default => sub { [qw( item article )] },
);

has md5hash => (
    is => 'ro',
    default => sub { +{ } },
);

method convert() {
    $self->output_dir->mkpath;

    for my $record_type (@{ $self->record_types }) {
        for my $query (@{ $self->queries }) {
            $self->convert_type_query($record_type, $query);
        }
    }

    $self->write_index;
}

method convert_type_query($record_type, $query) {
    $self->api->search(
        query       => $query,
        record_type => $record_type,
        per_page    => 100,
        callback    => fun($record) { $self->convert_record($record) },
    );
}

method convert_record($record) {
    my $type = $record->{recordType};
    my (undef, $id) = split('/', $record->{id});
    my $filename = "museumvic-$type-$id.xml";

    my $xml = json_to_xml($record, 'record');
    $self->output_dir->child($filename)->spew_raw($xml);

    $self->md5hash->{$filename} = md5_hex($xml);
}

method write_index() {
    my $fh = $self->output_dir->child('resources.xml')->openw_raw;
    my $writer = XML::Writer::Simple->new(fh => $fh);

    my $md5hash = $self->md5hash;

    $writer->open(resources => {
        recordCount => scalar keys %$md5hash,
    });

    for my $filename (sort keys %$md5hash) {
        $writer->open(record => {});

        $writer->element(name => $filename);
        $writer->element(hash => $md5hash->{$filename} );

        $writer->close;
    }

    $writer->close;
}

1;
