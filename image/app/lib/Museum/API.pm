package Museum::API;

use 5.24.0;
use warnings;

use Cpanel::JSON::XS        qw( );
use Gzip::Faster            qw( gunzip );
use HTTP::Tiny              qw( );
use Log::Any                qw( $log );
use URL::Builder            qw( build_url );

use Moo;

use Function::Parameters    qw( :strict );

has ua => (
    is => 'lazy',
    builder => sub {
        HTTP::Tiny->new(
            default_headers => { 'Accept-Encoding' => 'gzip' },
        ),
    },
);

has json => (
    is => 'lazy',
    builder => sub { Cpanel::JSON::XS->new },
);

has base_url => (
    is => 'ro',
    default => sub {
        'https://collections.museumvictoria.com.au/api',
    },
);

method url($path, $parameters = { }) {
    return build_url(
        base_uri => $self->base_url,
        path => "/$path",
        query => [ %$parameters ],
    );
}

method search(:$query, :$record_type, :$callback, :$per_page = 40) {
    for (my $page = 1; ; $page++) {
        my $url = $self->url(search => {
            query       => $query,
            recordtype  => $record_type,
            perpage     => $per_page,
            page        => $page,
        });

        $log->debug("Fetching $record_type($query) page $page");

        my $results = $self->get_json($url);
        last if @$results == 0;

        for my $result (@$results) {
            $callback->($result);
        }
    }
}
;
my %decode_content = (
    gzip => \&gunzip,
);

fun _decode_content($response) {
    my $encoding = $response->{headers}->{'content-encoding'}
        or return;

    if (my $decode = $decode_content{$encoding}) {
        my $old = length $response->{content};
        delete $response->{headers}->{'content-encoding'};
        $response->{content} = $decode->($response->{content});
#        my $new = length $response->{content};
#        my $percent = 100 * ($new - $old) / $new;
#        say STDERR "$old -> $new $percent%";
        return;
    }

    warn "Unexpected content-encoding: $encoding";
}

method get_json($url) {
    #$log->debug("GET $url");
    my $response = $self->ua->get($url);

    if (!$response->{success}) {
        die "Fetching $url : $response->{status} $response->{reason} $response->{content}\n";
    }
    _decode_content($response);

    return $self->json->decode($response->{content});
}

1;
